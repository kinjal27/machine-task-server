import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import multer from 'multer';
import fs from 'fs';

const app = express();
const router = express.Router();
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.use(cors());

mongoose.connect('mongodb://localhost:27017/myMachineTask', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => {
    console.log('database connected');
});

const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String
});

const User = new mongoose.model("User", userSchema);

const fileuploadSchema = new mongoose.Schema({
    uploadedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    filePath: String,
    fileName: String,
    permittedUsers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

const FileUpload = new mongoose.model("FileUpload", fileuploadSchema);

const genToken = (id) => {
    return jwt.sign({id}, '123', {
        expiresIn: '30d'
    })
}

const findUser = (email) => {
    return User.findOne({email: email})
}
const findFile = (id) => {
    return FileUpload.findOne({_id: mongoose.Types.ObjectId(id)})
}
const findAllFile = () => {
    return FileUpload.find({})
}
const findAllUser = () => {
    return User.find({})
}

const protect = async (req, res, next) => {
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        try {
            token = req.headers.authorization.split(' ')[1];
            const decoded = await jwt.verify(token, '123');
            req.user = await User.findById(decoded.id).select('-password');
            next();
        } catch(err) {
            res.status(401);
            throw new Error('Not Authorized');
        }
    }
    if (!token) {
        res.status(401);
        throw new Error('Not Authorized');
    }
}

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads/');
    },
    filename: function (req, file, callback) {
        const fileExtension = file.originalname.split(".")[1];
        const fullFileName = `${file.fieldname}${Math.random(10)}.${fileExtension}`.toLowerCase();
        callback(null, fullFileName);
    }
});
const upload = multer({ storage });
app.post('/login', async (req, res) => {
    const { email, password } = req.body;

    const userFindOne = await findUser(email);
    if (!userFindOne) {
        return res.status(400).send({ message:'User not registered' });
    }
    if (await bcrypt.compare(password, userFindOne.password)) {
        userFindOne.token = genToken(userFindOne._id);
        return res.status(200).send({message: 'User login successfully', 
            user: {
                _id: userFindOne.id,
                email: userFindOne.email,
                name: userFindOne.name,
                token: genToken(userFindOne._id),
            }
        });
    }
    return res.status(400).send({message: 'Wrong password'});
});

app.post('/register', async (req, res) => {
    const { name, email, password } = req.body;

    const userFindOne = await findUser(email);
    if (userFindOne) {
        return res.status(404).send({message:'User already exist'});
    }
    const bSalt = await bcrypt.genSalt(10);
    const crypePass = await bcrypt.hash(password, bSalt);
    const user = await new User({
        name,
        email,
        password: crypePass,
    });
    user.save((err, userSave) => {
        if (err) {
            return res.status(404).send({message: 'User registration Failed'});
        } else {
            return res.status(200).json({
                message:'User register successfully',
                user: {
                    _id: userSave.id,
                    email: userSave.email,
                    name: userSave.name,
                    token: genToken(userSave._id),
                }
            })
        }
    });
});
app.all('/v1/*', protect);

app.post("/v1/uploadFile", upload.single("uploadFile"), async (req, res) => {
    if (!req.file || !req.file.fieldname) {
        return res.status(409).send({ error: 'Something went wrong' });
    }
    fs.readFile(req.file.path, async function (err, data) {
        if (err) {
            return res.status(409).send({ error: 'Error occurred in reading the file' });
        }
        const fileUpload = await new FileUpload({
            uploadedBy: req.user.id,
            fileName: req.file.originalname,
            filePath: './uploads' + req.file.path,
            permittedUsers: req.body.permittedUsers ? req.body.permittedUsers : null,
        });
        fileUpload.save((err, fileUploadSave) => {
            if (err) {
                return res.status(404).send({message: 'File upload Failed'});
            } else {
                return res.status(200).json({
                    message:'File upload successfully',
                    fileName: req.file.originalname
                })
            }
        });
    });
});

app.get('/v1/listFile', async (req, res) => {
    const files = await findAllFile();
    if (!files || !files.length) {
        return res.status(204).send({message:'Files not found'});
    }
    return res.status(200).send({message: 'Files retrieved successfully', files});
});

app.get('/v1/listUser', async (req, res) => {
    const users = await findAllUser();
    if (!users || !users.length) {
        return res.status(204).send({message:'Users not found'});
    }
    return res.status(200).send({message: 'Users retrieved successfully', users});
});

app.get('/v1/fileDetail', async (req, res) => {
    const { userId, fileId } = req.body;
    const file = await findFile(fileId);
    console.log(file);
    if (!file) {
        return res.status(404).send({message:'File not found'});
    }
    if (file.uploadedBy === userId || file?.permittedUsers?.includes(userId)) {
        fs.readFile(file.filePath, function (err, data) {
            if (err) {
                return res.status(409).send({ error: 'Error occurred in reading the file' });
            }
            return res.status(200).send({message: 'File retrieved successfully', data});
        });
    } else {
        return res.status(403).send({ message: 'You are not authorized to view the file.'})
    }
});

app.listen(5000, () => {
    console.log('server is running on port number 5000');
});